---
marp: true
theme: uncover
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---
<style scoped>
h1 {
  font-size: 70px;
}
</style>

# REST
# REpresentational State Transfer

---

- [Defined](https://roy.gbiv.com/pubs/dissertation/top.htm) by HTTP 1.0 & 1.1 author (Roy Fielding)
- Architectural style
- REST = Transfer of representations of resources
- A simple way to handle interactions between systems (KISS)
- Common standard in the industry is to design RESTful, Business oriented & Open Ready APIs

---

# Pros & Cons

- Simple: Leverages HTTP Protocol
  - Well known, widely deployed 
  - Use HTTP verbs
  - Use URI as a global identifier for resources
- Readable: Uniform Interface
  - Consistant interface accross all APIs.
  - Easy to read and understand by a human
- Portable & Flexible : Adapts very well to different platform and use cases
- Scalable: Statelessness -> Easy horizontal scaling
- Independence: Separation between client and server, allow for autonomous development accross sections of a project.

---

# Set of constraints

`When given a name, a coordinated set of
architectural constraints becomes an architectural style`

- Client-Server
- Uniform Interface
- Stateless
- Cacheable
- Layered System
- Code on Demand

---

# Client-Server

- A Client is someone who is requesting resources and are not concerned with data storage, which remains internal to each server
- Server is someone who holds the resources and are not concerned with the user interface or user state
- Separation of concerns, separate interface from implementation. 
- Allow components to evolve independently.

---

# Uniform Interface

- Identification of resources: each resource is uniquely identified in requests
- Manipulation of resources through representations: each resource has one or more representations
- Self-descriptive message: message is not only data but everything necessary for the message to be processed
- Hypermedia as the engine for application state (HATEOAS): the server must give the client the needed information to navigate the service

---

# Stateless

- The server will not store anything about the latest HTTP request the client made
- Requests contains all necessary information
- No session
- No history
- The client is responsible for managing the state of the application.

--- 

# Cacheable

- Responses include whether the response is cacheable or not
- If cached, Client will return data from cache withtout the need to send the request again to the server
- Improves performance
- Risks of stale data 

---

# Layered System

- Client has no idea about the end server or intermediates processing the requests, only the immediate layer with wich it interacts with
- Intermediary servers may improve system availability by enabling load-balancing and by providing shared caches
- The disadvantage is that they add overhead to the processing of data, which could reduce performance

---

# Code on Demand (Optional)

- Client are extendable by downloading executable code

---

# Rules of RESTful API

---

# Resources

- Should prefer naming resource with noun instead of verbs (!= RPC)
- Plural: /dogs
- Concrete: /dogs instead of /animals
- Use Javascript naming convention
- Prefer hyphens (-) to underscores(_) for readability
- User lowercase

---

# CRUD

Implements CRUD using HTTP Methods

- POST = Create
- GET = Read
- PUT = Update
- DELETE = Delete

---

# Collections

- 2 base URLs per resource
  - Collection
  - Element

```
GET /dogs
GET /dogs/{id}
```

<!---
IDs are preferaby functional
Unicity of IDs is requiered
---->

---

# Hierarchical structure 

- Use forward slash (/) to indicate hierarchical relationships
```
GET /dogs
GET /dogs/{id}
GET /dogs/{id}/tricks
GET /dogs/{id}/tricks/{id}
```

---

# Query strings

- Filters
  - `GET /dogs?race=golden-retriever`
- Pagination
  - Pagination is mandatory
  - Default pagination should be defined (Ex: 0-25)
  - `GET /dogs?per_page=50&page=2`

---

# Partial responses

- Should support partial responses so that developers can select needed information
- Important when bandwidth need to be optimized (for example for mobile development)
- `GET /dogs/125?fields=race,name,owner`

---

## Requests for non-resources

- Use verbs for non-resources: compute, search, …

```
GET /owners/5678/dogs/search?q=toto
GET /dogs/count
```

---

# I18N

- Use ISO 8601 standard for Date/Time/Timestamp
  - `2022-11-28 15:00:00.000`
  - `2022-11-28`
- Add support for different languages
  - `Accept-Language: fr-CA, fr-FR` 
  - Not `?language=fr`

---

# Content Negotiation

- Client asks for resources, in Accept header in order of preference
  - `Accept: application/json, text/plain`
  - Default is JSON

---
<style scoped>
h1 {
    font-size: 50px;
}
li {
    font-size: 30px;
}
code {
  font-size: 20px;
}
</style>

# HATEOAS

- Use [RFC5988](https://www.rfc-editor.org/rfc/rfc5988) link notation to implement HATEOAS

```
{
    "dog": {
        "id": 243
        "name": "Wan",
        "race": "Golden Retriever",
        "owner": "John",
        "age": 5
    },
    "_links": [
        {
            "href": "https://example.org/api/v1/owners/135",
            "rel": "owner",
        }
    ]
}
```

<!---
Hypermedia as the Engine of Application State
--->

---
 
# APIs as contracts

- To ensure low coupling between components, API changes should not require consumers to be changed.
- The implementation can change, but not the interface
- If the contract must be broken(introducting a *breaking change*), you should create a new API while temporarly keeping the old one, for retro-compatibility

---

# Versioning

- To support breaking changes, APIs should be versioned

```
PATCH /api/v1/dogs '{"owner": "John"}'
PATCH /api/v2/dogs '{"favouriteHuman": "John"}'
```

---

# JavaScript Object Notation (JSON)

- Lightweight standard for data-interchange format
- Developer-friendly
    - Easy for humans to read/write
    - Efficient to parse/generate
- Open, not only Javascript

---

# JSON Types

- Number: 12 2.45
- String: "hello" 'hello'
- Boolean: true false
- Empty value: null
- Array: ordered values: [1, "apple", true, {...}]
- Object: unordered key/value pairs: {"title": Game of Thrones", "season": 1}


---

# API First

- Start by defining the contract (define the API) before starting any development
- Use standard and normalized format (OpenAPI)
- Allow development teams to work in parallel
- As the API developer, i can test that my service implements properly the contract

---

# Practical Work

---

- Go to https://studio.apicur.io
- Create an account
- Create a new API and use the *Pet Store Example* Template
- Explore the "Preview Documentation"
- Explore the "Edit API", note how the example is designed and the raw definition (Source)

---

<style scoped>
li {
  font-size: 30px;
}
</style>

- Using the pet store example as a base, we will design a new API
- Create a new API **Patient**
- The patient resource should have the following properties:
  - ID
  - Name
  - Birth date
  - Sex
  - Social security number
- Design RESTful API endpoints giving the following capabilities:
  - List patients
  - Retrieve a specific patient informations
  - Add a new patient
  - Delete a patient
  - Update a patient
  - Search patient by Social security number

---

- Push your OpenAPI specification to the Gitlab repository we created in the HTTP asignment.
- You can either
  - Export the spec, commit and push it to your Gitlab repository
  - Link your Apicurio account to your Gitlab's and use Apicurio "publish" feature to directly commit it in your Gitlab Repository